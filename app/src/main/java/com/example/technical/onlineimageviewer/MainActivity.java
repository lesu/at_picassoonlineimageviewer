package com.example.technical.onlineimageviewer;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;


public class MainActivity extends ActionBarActivity {
    private EditText editTextIn;
    private Button buttonView;
    ImageView imageView;
    private static final String IMAGE_URL = "http://image.tmdb.org/t/p/w185/k1QUCjNAkfRpWfm1dVJGUmVHzGv.jpg";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.init();
    }

    private void init() {
        this.editTextIn = (EditText) this.findViewById(R.id.main_edit_text_in);
        this.buttonView = (Button) this.findViewById(R.id.main_button_view);
        this.imageView = (ImageView) this.findViewById(R.id.main_image_view);

        this.buttonView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = MainActivity.this.editTextIn.getText().toString();
//                Picasso.with(MainActivity.this).load(url).into(MainActivity.this.imageView);
                Picasso.with(MainActivity.this).load(MainActivity.IMAGE_URL).into(MainActivity.this.imageView);

            }
        });

    }
}
