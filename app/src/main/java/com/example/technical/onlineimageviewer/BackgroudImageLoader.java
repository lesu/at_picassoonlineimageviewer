package com.example.technical.onlineimageviewer;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

class BackgroudImageLoader extends AsyncTask<String,Void,Bitmap>{
    private MainActivity callingActivity;

    BackgroudImageLoader(MainActivity callingActivity) {
        this.callingActivity = callingActivity;
    }

    @Override
    protected Bitmap doInBackground(String... params) {
        Bitmap bitmap = null;
        try {
            bitmap = BitmapFactory.decodeStream((InputStream) new URL(params[0]).getContent());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bitmap;
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        if(bitmap!=null){
            this.callingActivity.imageView.setImageBitmap(bitmap);
        }
    }
}
