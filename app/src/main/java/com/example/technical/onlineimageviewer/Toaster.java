package com.example.technical.onlineimageviewer;

import android.content.Context;
import android.widget.Toast;

class Toaster {
    private static volatile Toast theToast;

    public static void showToast(Context context,String text,boolean longDuration){
        Toaster.cancelLast();
        int duration = (longDuration) ? Toast.LENGTH_LONG : Toast.LENGTH_SHORT;
        theToast.makeText(context,text,duration).show();
    }

    private static void cancelLast() {
        if(Toaster.theToast!=null) {
            Toaster.theToast.cancel();
        }
    }
}
